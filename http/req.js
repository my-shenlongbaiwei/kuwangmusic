// 基本路径
const baseURL="https://netease-cloud-music-api-amber-sigma.vercel.app"
const cookies=wx.getStorageSync('cookies')[0]

// loading效果记次数
let num=0

const request=(params)=>{
    // 当发送请求数字开始++
    num++
    // load效果
    wx.showLoading({
        title: '加载中',
      })
    //   使用prominse封装
    return new Promise((resolve,reject)=>{
        // 使用微信的自带的request
       wx.request({
        //    params是参数
            ...params,
            // url是地址
            url:baseURL+params.url,
            header: {
                'cookie': cookies // 设置cookie
              },
            // 请求成功
            success:(res)=>{
                resolve(res)
            },
            // 请求失败
            fail(err){
                reject(err)
            },
            complete(){
                // 请求完成num--
                num--
                // 如果为0，则取消load效果
                if(num==0){
                    wx.hideLoading()
                }
            } 
       })
    })
}
// 导出请求
export default request