// 引入文件
// 推荐歌单
import http from './req'
// 函数

function getlist(params){
    // 调用请求并返回数据
    return http({
        // 接口
        url:"/personalized",
        // 请求方式
        method:"get",
        // 参数
        data:params
    })
}
// 导出方法
export {getlist}