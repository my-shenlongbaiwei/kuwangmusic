// 引入文件
// 推荐歌单
import http from './req'
// 函数


function gethot(params){
    // 调用请求并返回数据
    return http({
        // 接口
        url:"/comment/hotwall/list",
        // 请求方式
        method:"get",
        // 参数
        data:params
    })
}
// 导出方法
export {gethot}