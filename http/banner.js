// 引入文件
// 首页轮播
import http from './req'
// 函数

function getBanner(params){
    // 调用请求并返回数据
    return http({
        // 接口
        url:"/banner",
        // 请求方式
        method:"get",
        // 参数
        data:params
    })
}
// 导出方法
export {getBanner}