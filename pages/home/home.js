// pages/home/home.js
import { getlist } from "../../http/Recommend the playlist";
import { getSong } from "../../http/Recommend songs"
import { getBanner } from "../../http/banner";
Page({
  /**
   * 页面的初始数据
   */
  data: {
        list:[],
        song:[],
        banner:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 推荐歌单
    const new_date = new Date().getTime()
    getlist({
      timestamp: new_date
    }).then((res) => {
      // console.log(res);
      this.setData({
        list: res.data.result
      })
        console.log(res.data.result);
    })
    // 排行榜
    getSong({timestamp:new_date}).then((res) =>{
      // console.log(res);
      this.setData({
        song:res.data.list.slice(0,5)
      })
      console.log(res.data.list);
    })

    // 首页轮播
    getBanner({timestamp:new_date}).then(res=>{
      // console.log(res);
      this.setData({
        banner:res.data.banners
      })
      console.log(res.data.banners);
    })
    const cookies = wx.getStorageSync('cookies')
    const uid = wx.getStorageSync('uid')

    getlists({
      timestamp: new_date,
      cookie: cookies,
      uid:uid
    }).then((res) => {
      console.log(res);
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})