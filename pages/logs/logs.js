// logs.js
import {gethot} from "../../http/logs"
Page({
  data: {
    list:[]

  },
  onLoad() {
    
    gethot({timestamp:new Date().getTime()}).then((res)=>{
      console.log(res);
      this.setData({
        list:res.data.result
      })
    })
  }
})
